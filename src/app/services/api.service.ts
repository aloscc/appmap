import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  server = 'https://www.mocky.io/v2/5d67028f330000a9a4449fb3';
  constructor(private http: HttpClient) { }
  getCoordenates(): Observable<any> {
    return this.http.get('https://rubens-gps-tracker.herokuapp.com/api/v1/device');
  }

  sendCookie(cookie): Observable<any> {
    return this.http.post('https://rubens-gps-tracker.herokuapp.com/api/v1/credential', cookie);
  }
}
