import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MapRoutingModule } from './map-routing.module';
import { GooglemapsComponent } from './googlemaps/googlemaps.component';
import { MapComponent } from './map.component';
import { FormsModule } from '@angular/forms';
import { LeafletmapsComponent } from './leafletmaps/leafletmaps.component';

@NgModule({
  declarations: [GooglemapsComponent, MapComponent, LeafletmapsComponent],
  imports: [
    CommonModule,
    MapRoutingModule,
    FormsModule
  ]
})
export class MapModule { }
