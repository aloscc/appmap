import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MapInterface } from '../../../models/map.interface';

@Component({
  selector: 'app-googlemaps',
  templateUrl: './googlemaps.component.html',
  styleUrls: ['./googlemaps.component.scss']
})
export class GooglemapsComponent implements OnInit, OnChanges {
  @Input() data: MapInterface;
  url: any;
  constructor(
    private sanitizer: DomSanitizer,
  ) { }

  ngOnInit() {
    if (this.data) {
      this.url =  this.sanitizer.bypassSecurityTrustResourceUrl('https://maps.google.com/maps?q='+ this.data.latitude + ',' + this.data.longitude +'&z=17&output=embed');  
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.data) {
      this.url =  this.sanitizer.bypassSecurityTrustResourceUrl('https://maps.google.com/maps?q='+ this.data.latitude + ',' + this.data.longitude +'&z=17&output=embed');
    }
  }
}
