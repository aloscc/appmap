import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MapInterface } from '../../../models/map.interface';

declare let L;
@Component({
  selector: 'app-leafletmaps',
  templateUrl: './leafletmaps.component.html',
  styleUrls: ['./leafletmaps.component.scss']
})
export class LeafletmapsComponent implements OnInit, OnChanges {
  @Input() data: MapInterface;
  theMarker = {};
  map: any;
  iconCar: any;
  constructor() {
    this.iconCar = L.icon.mapkey({icon:'bus', color:'#004f99', background:'#f2c357', size:30});
    /*this.iconCar = L.icon({
      iconUrl: 'assets/icons/car2.png',
      iconSize:     [49, 49], // size of the icon
      iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
      popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
    });*/ 
  }

  ngOnInit() {
    this.map = L.map('map').setView([this.data.latitude, this.data.longitude], 17);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'}).addTo(this.map);
    this.theMarker = L.marker([this.data.latitude, this.data.longitude], {icon: this.iconCar}).addTo(this.map)
      .bindPopup('Dispositivo GPS 1')
      .openPopup();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (!changes.data.firstChange) {
      this.map.removeLayer(this.theMarker);
      this.theMarker = L.marker([this.data.latitude, this.data.longitude], {icon: this.iconCar}).addTo(this.map)
        .bindPopup('Dispositivo GPS 1')
        .openPopup();
    }
  }
}
