import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { MapInterface } from '../../models/map.interface';
import { repeat, delay } from 'rxjs/operators';
import { mergeMap } from 'rxjs/operators';
import { fromEvent, iif, of, interval, pipe } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  oppoSuits = ['GoogleMaps', 'LeafletMaps'];
  currentvalueselect = '';
  data: MapInterface;
  url;
  counter = 0;
  constructor(
    private api: ApiService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.currentvalueselect = 'GoogleMaps';
    interval(1000).pipe(
      mergeMap(v =>
        iif(
          () => v % 10 === 0,
          this.api.getCoordenates(),
          of(v)
        ))
    ).subscribe(data => {
      if (Number.isInteger(data)) {
        this.counter = data % 10;
      } else {
        if (data.latitude) {
          this.data = data;
        } else {
          this.router.navigateByUrl('login');
        }
      }
    });
  }

  changeSelect(e) {
    this.currentvalueselect = e.target.value;
  }
}
