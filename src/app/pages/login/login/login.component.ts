import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Globals } from '../../../globals';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  inputcookie = '';
  constructor(
    private router: Router,
    private globals: Globals,
    private api: ApiService
  ) { }

  ngOnInit() {
  }

  goto() {
    if (this.inputcookie.length > 0) {
      this.api.sendCookie(this.inputcookie).subscribe(res => {
        if (res) {
          this.router.navigateByUrl('maps');
        }
      });
    }
  }
}
