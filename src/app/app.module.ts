import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ApiService } from './services/api.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { MapModule } from './pages/map/map.module';
import { LoginModule } from './pages/login/login.module';
import { Globals } from './globals';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    MapModule,
    LoginModule
  ],
  providers: [ ApiService, Globals ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }

