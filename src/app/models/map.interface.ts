export interface MapInterface {
  latitude: string,
  longitude: string,
  speed: string,
  serverUtcDate: string,
  deviceUtcDate: string 
}

